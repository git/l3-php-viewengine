<?php

namespace elanpl\L3\PHPViewEngine;

class PHPViewEngine implements \elanpl\L3\IViewEngine{

    public $_L3;

    public function __construct($_L3)
    {
        $this->_L3 = $_L3;
    }
    
    public function render($view, $context){
        ob_start();
        include($view);
        $buf = ob_get_contents();
        ob_end_clean();

        return $buf; 
    }

}